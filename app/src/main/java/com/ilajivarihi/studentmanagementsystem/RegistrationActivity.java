package com.ilajivarihi.studentmanagementsystem;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

public class RegistrationActivity extends AppCompatActivity {

    EditText txtAdmission, txtName, txtEmail, txtPhone, txtAddress;
    ProgressBar progressBar;
    Spinner spinner;
    private TextView tvDate;
    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayList<Long> arrayIdList = new ArrayList<>();
    Button btnRegister;
    private long classRoomItemId = 0;
    private Integer defaultRole = 0;
    private FirebaseAuth mFirebaseAuthNew;
    TextView classId;
    private DatabaseReference dbRegReff;
    private Member member;
//    private long maxId = 0;

    private DatePickerDialog.OnDateSetListener mDateSetListener;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        dbRegReff = FirebaseDatabase.getInstance().getReference().child("Member");

        FirebaseOptions firebaseOptions = new FirebaseOptions.Builder()
                .setDatabaseUrl("[Database_url_here]")
                .setApiKey("AIzaSyDs8lYrybVtpYAt0T5saqSxTReGRFQ9SDU")
                .setApplicationId("1:1099007149692:android:6b80a5cegh7b124ddfab9f").build();

        try { FirebaseApp myApp = FirebaseApp.initializeApp(getApplicationContext(), firebaseOptions, "TempApp");
            mFirebaseAuthNew = FirebaseAuth.getInstance(myApp);
        } catch (IllegalStateException e){
            mFirebaseAuthNew = FirebaseAuth.getInstance(FirebaseApp.getInstance("TempApp"));
        }
//        dbRegReff.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot snapshot) {
//                if (snapshot.exists()) {
//                    maxId = snapshot.getChildrenCount();
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError error) {
//
//            }
//        });

        txtAdmission = (EditText)findViewById(R.id.txtAdmission);
        txtName = (EditText)findViewById(R.id.txtName);
        txtEmail = (EditText)findViewById(R.id.txtEmail);
        txtPhone = (EditText)findViewById(R.id.txtPhone);
        txtAddress = (EditText)findViewById(R.id.txtAddress);
        tvDate = (TextView)findViewById(R.id.txtDate);
        classId = (TextView)findViewById(R.id.classId);
        spinner = (Spinner) findViewById(R.id.spinner);
        classId.setVisibility(View.GONE);
        spinner.setVisibility(View.GONE);
        member = new Member();
//        HashFunction hashFunction = new HashFunction();
        btnRegister = findViewById(R.id.btnRegister);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        if (getIntent().getIntExtra("roll", defaultRole) == 4) {
            getRooms();
            spinner.setVisibility(View.VISIBLE);
            classId.setVisibility(View.VISIBLE);
        }

        btnRegister.setOnClickListener(v -> {
            String admitNo = txtAdmission.getText().toString();
            String name = txtName.getText().toString();
            String email = txtEmail.getText().toString().trim();
            String phone = txtPhone.getText().toString().trim();
            String address = txtAddress.getText().toString();
            String dob = tvDate.getText().toString();

            member.setAdmit_no(admitNo);
            member.setName(name);
            member.setEmail(email);
            member.setDate_of_birth(dob);
            member.setPhone(!phone.isEmpty() ? Long.parseLong(phone) : 0);
            member.setAddress(address);
            member.setClass_room_id(classRoomItemId);
            member.setMember_roll(getIntent().getIntExtra("roll", defaultRole));
//                member.setHidden(disable);
//                member.setDeleted(delete);
//                member.setClass_id(null);
            if (admitNo.isEmpty()) {
                txtAdmission.setError("Please Enter Admission Number");
                txtAdmission.requestFocus();
            }
            else if (name.isEmpty()) {
                txtName.setError("Please Enter Name");
                txtName.requestFocus();
            }
            else if (email.isEmpty()) {
                txtEmail.setError("Please Enter Email Address");
                txtEmail.requestFocus();
            }
            else if (!(Patterns.EMAIL_ADDRESS.matcher(email).matches())){
                txtEmail.setError("Invalid Email");
                txtEmail.requestFocus();
            }
            else if (dob.isEmpty()) {
                tvDate.setError("Please Enter Date of birth");
                tvDate.requestFocus();
            }
            else if (admitNo.isEmpty() && name.isEmpty() && email.isEmpty()) {
                Toast.makeText(RegistrationActivity.this, "Error occured! ", Toast.LENGTH_SHORT).show();
            } else {
                if (isNetworkConnected()) {
                    Toast.makeText(RegistrationActivity.this,"No Internet", Toast.LENGTH_SHORT).show();
                }
                else {
                    progressBar.setVisibility(View.VISIBLE);
                    mFirebaseAuthNew.createUserWithEmailAndPassword(email, email).addOnCompleteListener(RegistrationActivity.this, task -> {
                        if(task.isSuccessful()) {
                            member.setMember_id(Objects.requireNonNull(mFirebaseAuthNew.getCurrentUser()).getUid());
//                            Toast.makeText(getApplicationContext(),"get Intent roll: " + member.getMember_roll(),Toast.LENGTH_SHORT).show();
                            dbRegReff.child(member.getMember_id()).setValue(member)
                                .addOnCompleteListener(task1 -> {

                                if (task1.isSuccessful()) {
                                    progressBar.setVisibility(View.GONE);
                                    finish();
                                    Toast.makeText(RegistrationActivity.this, "Registeration Success", Toast.LENGTH_SHORT).show();
                                    mFirebaseAuthNew.signOut();
                                }
                                else {
                                    progressBar.setVisibility(View.GONE);
                                    mFirebaseAuthNew.signOut();
                                    Toast.makeText(RegistrationActivity.this, Objects.requireNonNull(task1.getException()).getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                        else {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(RegistrationActivity.this, Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_SHORT).show();
//                                startActivity(new Intent(RegistrationActivity.this, MainActivity.class));
                        }
                    });
                }
//                    Toast.makeText(RegistrationActivity.this, "Test" + set, Toast.LENGTH_SHORT).show();
            }
        });

        tvDate.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dialog = new DatePickerDialog(RegistrationActivity.this,
                    android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                    mDateSetListener,
                    year,month,day
            );
            Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        });

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        mDateSetListener = (view, year, month, dayOfMonth) -> {
            month = month + 1;
//                Log.d(TAG, "onDateSet: date " + year + "/" + month + "/" + dayOfMonth);
            tvDate.setText(year + "/" + month + "/" + dayOfMonth);
        };

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Spinner sp = (Spinner)parent;
                if (sp.getId() == R.id.spinner) {
                    classRoomItemId = arrayIdList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void createRoom() {
        ArrayAdapter arrayAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, arrayList);
        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);

    }

    private void getRooms() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("ClassRoom").orderByChild("deleted").equalTo(0)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        arrayList.clear();
                        arrayIdList.clear();
                        arrayList.add("---Choose Class---");
                        arrayIdList.add((long)0);
                        for (DataSnapshot item : snapshot.getChildren()) {
                            ClassRoom classRoom = item.getValue(ClassRoom.class);
                            if (Objects.requireNonNull(classRoom).getHidden() == 0) {
                                arrayList.add(classRoom.getRoom_name());
                                arrayIdList.add(classRoom.getRoom_id());
                            }
                        }
                        createRoom();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        Toast.makeText(RegistrationActivity.this, "Error Occurred! " + error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(!(cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected())) {
            Toast.makeText(RegistrationActivity.this,"No Internet", Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }
}