package com.ilajivarihi.studentmanagementsystem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.security.MessageDigest;

public class MainActivity extends AppCompatActivity {

    EditText txtEmail, txtPassword;
    Button btnLogin;
    ProgressBar progressBar;
//    private DatabaseReference dbReff;
    TextView tvReg;
    private FirebaseAuth mFirebaseAuth;
    private SharedPreferences session;
    private SharedPreferences.Editor editor;
//    private FirebaseAuth.AuthStateListener mAuthStateListener;
//    MessageDigest md;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFirebaseAuth = FirebaseAuth.getInstance();
        session = getSharedPreferences("member", MODE_PRIVATE);
        editor = session.edit();

        txtEmail = (EditText)findViewById(R.id.txtEmail);
        txtPassword = (EditText)findViewById(R.id.txtPassword);
        tvReg = findViewById(R.id.textView);
        tvReg.setVisibility(View.GONE);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        btnLogin = findViewById(R.id.btnLogin);
//        checkSession();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = txtEmail.getText().toString().trim();
                String pwd = txtPassword.getText().toString();
                if (email.isEmpty()) {
                    txtEmail.setError("Please Enter Username");
                    txtEmail.requestFocus();
                }
                else if (pwd.isEmpty()) {
                    txtPassword.setError("Please Enter Password");
                    txtPassword.requestFocus();
                }
                else if (email.isEmpty() && pwd.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Fields are empty", Toast.LENGTH_SHORT).show();
                }
                else if (!(email.isEmpty() && pwd.isEmpty())) {
                    if(isNetworkConnected()) {
                        return;
                    }

                    progressBar.setVisibility(View.VISIBLE);
                    mFirebaseAuth.signInWithEmailAndPassword(email, pwd).addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (!task.isSuccessful()) {
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(MainActivity.this,"Login Error!," + task.getException().getMessage() , Toast.LENGTH_SHORT).show();
                            }
                            else {
//                                finish();
                                DatabaseReference dbReff = FirebaseDatabase.getInstance().getReference("Member").child(mFirebaseAuth.getCurrentUser().getUid());
                                Intent intent = new Intent(getApplicationContext(),HomeActivity.class);
                                dbReff.addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                        progressBar.setVisibility(View.GONE);
                                        int roll = snapshot.child("member_roll").getValue(Integer.class).intValue();
//                                        getIntent().putExtra("roll",roll);
                                        intent.putExtra("roll",roll);
                                        editor.putString("permission", "2");
                                        startActivity(intent);
//                                        Toast.makeText(getApplicationContext(),"Error " + roll, Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {
                                        progressBar.setVisibility(View.GONE);
                                        Toast.makeText(getApplicationContext(),"Error occured" + error.getMessage(), Toast.LENGTH_SHORT).show();
                                        mFirebaseAuth.signOut();
                                    }
                                });
                            }
                        }
                    });
                }
                else {
                    Toast.makeText(MainActivity.this, "Error Occured!", Toast.LENGTH_SHORT).show();
                }
            }
        });

//        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();
//                if (mFirebaseUser != null) {
//                    Toast.makeText(MainActivity.this,"You are logged in", Toast.LENGTH_SHORT).show();
//                    startActivity(new Intent(MainActivity.this, HomeActivity.class));
//                }
////                else {
////                    Toast.makeText(MainActivity.this,"Please Login", Toast.LENGTH_SHORT).show();
////                }
//            }
//        };

        tvReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkConnected()) {
                    Toast.makeText(MainActivity.this,"No Internet", Toast.LENGTH_SHORT).show();
                    return;
                }
                startActivity(new Intent(MainActivity.this, RegistrationActivity.class));
            }
        });
    }
    protected void checkSession() {
        if (mFirebaseAuth.getCurrentUser() != null) {
            finish();
            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
        }
    }
    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(!(cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected())) {
            Toast.makeText(MainActivity.this,"No Internet", Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkSession();
//        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
        isNetworkConnected();
    }
}