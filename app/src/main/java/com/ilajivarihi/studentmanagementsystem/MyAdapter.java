package com.ilajivarihi.studentmanagementsystem;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private ArrayList<ClassSubject> arrayList;
    private Context context;
    private int classOpt;

    public MyAdapter(Context ct, ArrayList<ClassSubject> arr1, int option) {
        context = ct;
        arrayList = arr1;
        classOpt = option;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.my_row,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
//        String id = String.valueOf(arrayList.get(position).getClass_id());
        holder.text1.setText(arrayList.get(position).getSubject_name());
//        Toast.makeText(context,"ROOM ID " + context,Toast.LENGTH_SHORT).show();
        if (classOpt == 0) {
            holder.mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, TAttendActivity.class);
                    intent.putExtra("classId", arrayList.get(position).getClass_id());
                    intent.putExtra("subId", arrayList.get(position).getSubject_id());
                    context.startActivity(intent);
                }
            });
            DatabaseReference dbReff = FirebaseDatabase.getInstance().getReference("ClassRoom").child(String.valueOf(arrayList.get(position).getClass_id()));
            dbReff.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    holder.text2.setText(snapshot.child("room_name").getValue(String.class));
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        } else if(classOpt == 1) {
            holder.text4.setText("Tutor");
            holder.mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, SAttendActivity.class);
                    intent.putExtra("classId", arrayList.get(position).getClass_id());
                    intent.putExtra("subId", arrayList.get(position).getSubject_id());
                    context.startActivity(intent);
                }
            });
            DatabaseReference dbReff = FirebaseDatabase.getInstance().getReference("Member").child(arrayList.get(position).getTutor_id());
            dbReff.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    holder.text2.setText(snapshot.child("name").getValue(String.class));
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
        holder.text3.setText(String.format("%s - %s", arrayList.get(position).getStartTime(), arrayList.get(position).getEndTime()));
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
//        return 0;
    }

    protected static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView text1, text2, text3, text4;
        ConstraintLayout mainLayout;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            text1 = itemView.findViewById(R.id.tvName);
            text2 = itemView.findViewById(R.id.tvTutor);
            text3 = itemView.findViewById(R.id.tvTime);
            text4 = itemView.findViewById(R.id.tutor);
            mainLayout = itemView.findViewById(R.id.mainRow);
        }
    }
}
