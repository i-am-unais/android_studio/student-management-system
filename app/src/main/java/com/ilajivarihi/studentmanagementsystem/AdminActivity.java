package com.ilajivarihi.studentmanagementsystem;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class AdminActivity extends AppCompatActivity implements View.OnClickListener {

    Button studentButton, tutorButton, classRoomButton, subjectButton, logoutButton;
    FirebaseUser mFirebaseUser;
    private SharedPreferences session;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        session = getSharedPreferences("member",MODE_PRIVATE);
        editor = session.edit();
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        studentButton = (Button)findViewById(R.id.student);
        tutorButton = (Button)findViewById(R.id.tutor);
        classRoomButton = (Button)findViewById(R.id.classRoom);
        subjectButton = (Button)findViewById(R.id.subject);
        logoutButton = (Button)findViewById(R.id.logout);
        checkAdminSession();
//        Toast.makeText(getApplicationContext(),"Oncreate",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getApplicationContext(),RegistrationActivity.class);
//        Toast.makeText(getApplicationContext(),"onClick ID: " + v.getId()+"    "+R.id.student,Toast.LENGTH_SHORT).show();
        if(v.getId() == R.id.tutor) {
//            Toast.makeText(getApplicationContext(),"Tutor",Toast.LENGTH_SHORT).show();
            intent.putExtra("roll", 3);
        }
        else if(v.getId() == R.id.student) {
            intent.putExtra("roll", 4);
//            Toast.makeText(getApplicationContext(),"Student",Toast.LENGTH_SHORT).show();
        }
        else if(v.getId() == R.id.classRoom) {
            intent = new Intent(getApplicationContext(),ClassRegisterActivity.class);
            intent.putExtra("admin", true);
//            Toast.makeText(getApplicationContext(),"Class Room",Toast.LENGTH_SHORT).show();
        }
        else if(v.getId() == R.id.subject) {
            intent = new Intent(getApplicationContext(),SubjectRegisterActivity.class);
            intent.putExtra("admin", true);
//            Toast.makeText(getApplicationContext(),"Subject",Toast.LENGTH_SHORT).show();
        }
        else if(v.getId() == R.id.logout) {
            FirebaseAuth.getInstance().signOut();
            editor.clear();
            editor.commit();
            intent = new Intent(getApplicationContext(),MainActivity.class);
        }
        else {
            intent.putExtra("roll", 0);
//            Toast.makeText(getApplicationContext(),"Normal",Toast.LENGTH_SHORT).show();
        }
        finish();
        startActivity(intent);
    }

    protected boolean checkAdminSession() {
        if (mFirebaseUser == null || session.getString("permission", "0") != "2") {
            FirebaseAuth.getInstance().signOut();
            editor.clear();
            editor.commit();
            finish();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            return false;
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkAdminSession();
        studentButton.setOnClickListener(this);
        tutorButton.setOnClickListener(this);
        classRoomButton.setOnClickListener(this);
        subjectButton.setOnClickListener(this);
        logoutButton.setOnClickListener(this);
    }
}