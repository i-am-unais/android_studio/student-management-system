package com.ilajivarihi.studentmanagementsystem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ClassRoom {
    private long room_id;
    private String room_name, crdate = currentDate(), from, to;
//    private Member[] room_members;
    private ClassSubject[] room_subjects;
    private Integer deleted = 0, hidden = 0, subject_ids;

    public ClassRoom() {
        this.currentDate();
    }

    public long getRoom_id() {
        return room_id;
    }

    public void setRoom_id(long room_id) {
        this.room_id = room_id;
    }

    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }

    public ClassSubject[] getRoom_subjects() {
        return room_subjects;
    }

    public void setRoom_subjects(ClassSubject[] room_subjects) {
        this.room_subjects = room_subjects;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Integer getHidden() {
        return hidden;
    }

    public void setHidden(Integer hidden) {
        this.hidden = hidden;
    }

    public String getCrdate() {
        return crdate;
    }

    public void setCrdate(String crdate) {
        this.crdate = crdate;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Integer getSubject_ids() {
        return subject_ids;
    }

    public void setSubject_ids(Integer subject_ids) {
        this.subject_ids = subject_ids;
    }

    protected String currentDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date dateObj = new Date();
        return dateFormat.format(dateObj);
    }
}
