package com.ilajivarihi.studentmanagementsystem;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class Member {
    private String member_id, name, email, address, admit_no, date_of_birth;
    private Long phone;
    private Integer hidden = 0, deleted = 0, member_roll = 0;
    private Long class_room_id;
    private ClassRoom class_room = null;

    public Member() {
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAdmit_no() {
        return admit_no;
    }

    public void setAdmit_no(String admit_no) {
        this.admit_no = admit_no;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public Integer getHidden() {
        return hidden;
    }

    public void setHidden(Integer hidden) {
        this.hidden = hidden;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Integer getMember_roll() {
        return member_roll;
    }

    public void setMember_roll(Integer member_roll) {
        this.member_roll = member_roll;
    }

    public Long getClass_room_id() {
        return class_room_id;
    }

    public void setClass_room_id(Long class_room_id) {
        this.class_room_id = class_room_id;
    }

    public ClassRoom getClass_room() {
        return class_room;
    }

    public void setClass_room(ClassRoom class_room) {
        this.class_room = class_room;
    }

//    public Member getRoomObj() {
//        DatabaseReference dbReff;
//        dbReff = FirebaseDatabase.getInstance().getReference("ClassRoom");
//        if (class_room_id != 0) {
//            Query query = dbReff.orderByChild("room_id").equalTo(class_room_id)
//                    .orderByChild("deleted").equalTo(0)
//                    .orderByChild("hidden").equalTo(0);
//            query.addListenerForSingleValueEvent(new ValueEventListener() {
//                @Override
//                public void onDataChange(@NonNull DataSnapshot snapshot) {
//                    if (snapshot.exists()) {
//                        class_room = snapshot.getValue(ClassRoom.class);
//                    }
//                }
//
//                @Override
//                public void onCancelled(@NonNull DatabaseError error) {
//
//                }
//            });
//        }
//        return this;
//    }

//    public void getMemberById(@NonNull String id) {
//        DatabaseReference dbReff;
//        FirebaseDatabase.getInstance().getReference("Member").child(id).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot snapshot) {
//                if (snapshot.exists()) {
//                    Member dataDB = snapshot.getValue(Member.class);
//                    name = dataDB.getName();
////                    this.setDate_of_birth(data.getDate_of_birth());
////                    this.setMember_roll(data.getMember_roll());
////                    this.setAdmit_no(data.getAdmit_no());
////                    this.setPhone(data.getPhone());
////                    this.setEmail(data.getEmail());
////                    this.setMember_id(data.getMember_id());
////                    this.setDeleted(data.getDeleted());
////                    this.setHidden(data.getHidden());
////                    this.setClass_room_id(data.getClass_room_id());
////                    this.setClass_room(data.getRoomObj().getClass_room());
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError error) {
//                error.getCode();
//            }
//        });
//        address = "hai address";
//    }
}
