package com.ilajivarihi.studentmanagementsystem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

public class SubjectRegisterActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private Spinner spinner, spinner2;
    private SharedPreferences session;
    private final static int DIALOG_FROM_TIME_PICKER = 0;
    private final static int DIALOG_TO_TIME_PICKER = 1;
    private DatabaseReference databaseReference;
//    private ArrayList<ClassRoom> arrayList = new ArrayList<>();
    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayList<Long> arrayIdList = new ArrayList<>();
    private ArrayList<String> tutorList = new ArrayList<>();
    private ArrayList<String> tutorIdList = new ArrayList<>();
    private long classRoomItemId;
    private  String timeSet, tutorItemId = "";
    private boolean admin;
    private List<String> dayArray = Arrays.asList("0", "0", "0", "0", "0", "0", "0");
    CheckBox cb1, cb2, cb3, cb4, cb5, cb6, cb7;
    Button btnSave;
    TextView txtSubjectName, txtFrom, txtTo;
    ProgressBar progressBar;
    FirebaseUser mFirebaseUser;
    Calendar calendar;
    long maxId = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject_register);

        session = getSharedPreferences("member", MODE_PRIVATE);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        calendar = Calendar.getInstance();
        admin = session.getString("permission", "0").equals("2");

        btnSave = findViewById(R.id.btnSave);
        progressBar = new ProgressBar(getApplicationContext());
        txtSubjectName = findViewById(R.id.txtSubjectName);
        txtFrom = findViewById(R.id.txtFrom);
        txtTo = findViewById(R.id.txtTo);
        spinner = findViewById(R.id.spinner);
        spinner2 = findViewById(R.id.spinner2);
        cb1 = findViewById(R.id.checkBoxSun);
        cb2 = findViewById(R.id.checkBoxMon);
        cb3 = findViewById(R.id.checkBoxTue);
        cb4 = findViewById(R.id.checkBoxWed);
        cb5 = findViewById(R.id.checkBoxThu);
        cb6 = findViewById(R.id.checkBoxFri);
        cb7 = findViewById(R.id.checkBoxSat);

        checkAdminSession();
        getRooms();
        getTutor();
//        progressBar.setVisibility(View.GONE);
//        createNew();

    }

    private void createRoom() {
        ArrayAdapter arrayAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, arrayList);
        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
    }

    private void getRooms() {
        databaseReference.child("ClassRoom").orderByChild("deleted").equalTo(0)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        arrayList.clear();
                        arrayIdList.clear();
                        arrayList.add("---Choose Class---");
                        arrayIdList.add((long)0);
                        for (DataSnapshot item : snapshot.getChildren()) {
                            ClassRoom classRoom = item.getValue(ClassRoom.class);
                            if (Objects.requireNonNull(classRoom).getHidden() == 0) {
                                arrayList.add(classRoom.getRoom_name());
                                arrayIdList.add(classRoom.getRoom_id());
                            }
                        }
                        createRoom();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        Toast.makeText(SubjectRegisterActivity.this, "Error Occurred! " + error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void getTutor() {
        databaseReference.child("Member").orderByChild("member_roll").equalTo(3)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        tutorList.clear();
                        tutorIdList.clear();
                        tutorList.add("---Choose Tutor---");
                        tutorIdList.add("");
                        for (DataSnapshot item : snapshot.getChildren()) {
                            Member member = item.getValue(Member.class);
                            if (Objects.requireNonNull(member).getHidden() == 0 && Objects.requireNonNull(member).getDeleted() == 0) {
                                tutorList.add(member.getName());
                                tutorIdList.add(member.getMember_id());
                            }
                        }
                        createTutor();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        Toast.makeText(SubjectRegisterActivity.this, "Error Occurred! " + error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void createTutor() {
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item,tutorList);
        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner2.setAdapter(arrayAdapter);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnSave) {
            setDays();
            generateSubjectId();
            saveSubject();
        }
        if (v.getId() == R.id.txtFrom) {
            setTime(DIALOG_FROM_TIME_PICKER);
        }
        else if (v.getId() == R.id.txtTo) {
            setTime(DIALOG_TO_TIME_PICKER);
        }
    }

    protected void setDays() {
        if (cb1.isChecked()) {
            dayArray.set(0, "1");
        } else {
            dayArray.set(0, "0");
        }
        if (cb2.isChecked()) {
            dayArray.set(1, "1");
        } else {
            dayArray.set(1, "0");
        }
        if (cb3.isChecked()) {
            dayArray.set(2, "1");
        } else {
            dayArray.set(2, "0");
        }
        if (cb4.isChecked()) {
            dayArray.set(3, "1");
        } else {
            dayArray.set(3, "0");
        }
        if (cb5.isChecked()) {
            dayArray.set(4, "1");
        } else {
            dayArray.set(4, "0");
        }
        if (cb6.isChecked()) {
            dayArray.set(5, "1");
        } else {
            dayArray.set(5, "0");
        }
        if (cb7.isChecked()) {
            dayArray.set(6, "1");
        } else {
            dayArray.set(6, "0");
        }
    }

    protected boolean checkAdminSession() {
        if (mFirebaseUser == null || !admin) {
            FirebaseAuth.getInstance().signOut();
            finish();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            return false;
        }
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner sp = (Spinner)parent;
        if (sp.getId() == R.id.spinner) {
            classRoomItemId = arrayIdList.get(position);
        } else if (sp.getId() == R.id.spinner2) {
            tutorItemId = tutorIdList.get(position);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        checkAdminSession();
        btnSave.setOnClickListener(this);
        txtFrom.setOnClickListener(this);
        txtTo.setOnClickListener(this);
        spinner.setOnItemSelectedListener(this);
        spinner2.setOnItemSelectedListener(this);
    }

    private TimePickerDialog.OnTimeSetListener mEndTime = new TimePickerDialog.OnTimeSetListener(){
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            timeSet = hourOfDay + " : " + minute;
            txtTo.setText(timeSet);
            txtFrom.setText(MessageFormat.format("{0} : {1}", (hourOfDay - 1) < 0 ? 23 : hourOfDay - 1, minute));
        }
    };

    private TimePickerDialog.OnTimeSetListener mStartTime = new TimePickerDialog.OnTimeSetListener(){
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            timeSet = hourOfDay + " : " + minute;
            txtFrom.setText(timeSet);
            txtTo.setText(MessageFormat.format("{0} : {1}", (hourOfDay + 1) == 24 ? 0 : hourOfDay + 1, minute));
        }
    };

    protected void setTime (int id) {
        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        final int minute = calendar.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog;
        if (id == 0) {
            timePickerDialog = new TimePickerDialog(SubjectRegisterActivity.this,
                    mStartTime, hour,minute,
                    android.text.format.DateFormat.is24HourFormat(getApplicationContext()));
//            timePickerDialog.show();
        } else {
            timePickerDialog = new TimePickerDialog(SubjectRegisterActivity.this,
                    mEndTime, hour,minute,
                    android.text.format.DateFormat.is24HourFormat(getApplicationContext()));
        }
        timePickerDialog.show();
    }

    public int saveSubject () {
        if (!isNetworkConnected(getApplicationContext())) {
            Toast.makeText(SubjectRegisterActivity.this,"No Internet", Toast.LENGTH_SHORT).show();
            return 0;
        }
        String name = txtSubjectName.getText().toString().trim();
        String from = txtFrom.getText().toString().trim();
        String to = txtTo.getText().toString().trim();
//        Toast.makeText(getApplicationContext(),"Name: "+name+"\n classId: "+classRoomItemId+"\nTutorId: "+tutorItemId+
//                "\nFrom: "+from+"\nTo: "+to,Toast.LENGTH_SHORT).show();
        if (name.isEmpty()) {
            txtSubjectName.setError("Enter Name");
            txtSubjectName.requestFocus();
        } else if (classRoomItemId == 0) {
            TextView errorText = (TextView)spinner.getSelectedView();
            errorText.setError("Select Class");
            errorText.requestFocus();
        } else if (tutorItemId.isEmpty()) {
            TextView errorText = (TextView)spinner.getSelectedView();
            errorText.setError("Select Class");
            errorText.requestFocus();
        } else if (from.isEmpty()) {
            txtFrom.setError("Enter Start Time");
            txtFrom.requestFocus();
        } else if (to.isEmpty()) {
            txtTo.setError("Enter End Time");
            txtTo.requestFocus();
        } else if (!(name.isEmpty() && from.isEmpty() && to.isEmpty() && classRoomItemId == 0 && tutorItemId.isEmpty())) {
            ClassSubject classSubject = new ClassSubject();
            classSubject.setSubject_name(name);
            classSubject.setStartTime(from);
            classSubject.setEndTime(to);
            classSubject.setDays(dayArray);
            classSubject.setClass_id(classRoomItemId);
            classSubject.setTutor_id(tutorItemId);
            classSubject.setSubject_id(maxId + 1);
            progressBar.setVisibility(View.VISIBLE);
            databaseReference.child("Subject").child(String.valueOf(maxId + 1)).setValue(classSubject)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Toast.makeText(getApplicationContext(),"Saved success fully",Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(),"Error! " + task.getException(),Toast.LENGTH_SHORT).show();
                        }
                        progressBar.setVisibility(View.GONE);
                    });
        } else {
            Toast.makeText(getApplicationContext(), "Error Occured!", Toast.LENGTH_SHORT).show();
        }
        return 1;
    }

    public void generateSubjectId () {
        databaseReference.child("Subject").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    maxId = snapshot.getChildrenCount();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public boolean isNetworkConnected(Context context) {
//        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
        if(context == null)  return false;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        return true;
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        return true;
                    }  else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)){
                        return true;
                    }
                }
            }
            else {
                try {
                    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                    if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                        Log.i("update_statut", "Network is available : true");
                        return true;
                    }
                } catch (Exception e) {
                    Log.i("update_statut", "" + e.getMessage());
                }
            }
        }
        Log.i("update_statut","Network is available : FALSE ");
        return false;
    }
}