package com.ilajivarihi.studentmanagementsystem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class HomeActivity extends AppCompatActivity {

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private Member member;
    Intent intent;
//    private HashFunction hashFunction;
//    private DatabaseReference dbReff;
    private SharedPreferences session;
    private Editor editor;
    private ListView listView;
    Button btnLogout;
    private Integer member_roll = 0;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    TextView tvNameText, tvAdmissionText, tvDobText, tvEmailText, tvPhoneText, tvAddressText;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        session = getSharedPreferences("member", MODE_PRIVATE);
        editor = session.edit();
        tvNameText = (TextView)findViewById(R.id.tvNameText);
        tvPhoneText = (TextView)findViewById(R.id.tvPhoneText);
        tvEmailText = (TextView)findViewById(R.id.tvEmailText);
        tvAddressText = (TextView)findViewById(R.id.tvAddressText);
        tvAdmissionText = (TextView)findViewById(R.id.tvAdmissionText);
        tvDobText = (TextView)findViewById(R.id.tvDobText);
//        member = new Member();
//        checkSession();

//        Toast.makeText(getApplicationContext(),"Roll: "+ getIntent().getIntExtra("roll", 100),Toast.LENGTH_SHORT).show();
        switch (getIntent().getIntExtra("roll", 0)) {
            case 1:
            case 2:
//                Toast.makeText(getApplicationContext(),"1 and 2",Toast.LENGTH_SHORT).show();
                intent = new Intent(getApplicationContext(),AdminActivity.class);
                intent.putExtra("admin", true);
                editor.putString("permission", "2");
                editor.commit();
                finish();
                startActivity(intent);
                break;
            case 3:
                editor.putString("permission", "3");
                editor.commit();
                finish();
//                Toast.makeText(getApplicationContext(),"3",Toast.LENGTH_SHORT).show();
                intent = new Intent(getApplicationContext(),TutorActivity.class);
                intent.putExtra("tutor", true);
                startActivity(intent);
                break;
            case 4:
                editor.putString("permission", "4");
                editor.commit();
                finish();
//                Toast.makeText(getApplicationContext(),"4",Toast.LENGTH_SHORT).show();
                intent = new Intent(getApplicationContext(),StudentActivity.class);
                intent.putExtra("student", true);
                startActivity(intent);
                break;
            default:
                Toast.makeText(getApplicationContext(),"Default",Toast.LENGTH_SHORT).show();
//                        intent.putExtra("userName", nameDB);
//                        intent.putExtra("dob", dobDB);
//                        intent.putExtra("admitNo", admitNoDB);
//                        intent.putExtra("email", emailDB);
//                        intent.putExtra("userId", memberIdDB);
//                        intent.putExtra("phone", phoneDB);
//                        intent.putExtra("address", addressDB);

////                        Toast.makeText(getApplicationContext(), "Student",Toast.LENGTH_SHORT).show();
//
//                        Toast.makeText(getApplicationContext(), "This is Student" + member.getName(), Toast.LENGTH_SHORT).show();
//                        startActivity(intent);
//                        startActivity(intent);
                break;
        }

//        DatabaseReference dbReff = FirebaseDatabase.getInstance().getReference("Member");
//        addListenerForSingleValueEvent
//        dbReff.child(mFirebaseUser.getUid()).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot snapshot) {
//                member = snapshot.getValue(Member.class);
//                showData();
////                Toast.makeText(getApplicationContext(), nameDB +" "+member.getMember_roll(),Toast.LENGTH_SHORT).show();
//
////                Toast.makeText(getApplicationContext(), "This is Student" + member.getName(), Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError error) {
//                Toast.makeText(getApplicationContext(), "Error Occured" + error.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//
//        });
//        Toast.makeText(getApplicationContext(), "Test hi " + member_roll,Toast.LENGTH_SHORT).show();

//        Query query = dbReff.orderByChild("member_id").equalTo(mFirebaseUser.getUid());
//
//        query.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot snapshot) {
//                if (snapshot.exists()) {
//                    Member user = snapshot.child(mFirebaseUser.getUid()).getValue(Member.class);
//                    Toast.makeText(getApplicationContext(), "eXIST sNAPSHOT", Toast.LENGTH_SHORT).show();
//                    member = user;
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError error) {
//                Toast.makeText(getApplicationContext(), "No get Record", Toast.LENGTH_SHORT).show();
//            }
//        });

//        Toast.makeText(getApplicationContext(), mFirebaseUser.getUid(),Toast.LENGTH_SHORT).show();
//        Toast.makeText(getApplicationContext(), member.getName(),Toast.LENGTH_SHORT).show();



//        Query query = dbReff.orderByChild("member_id").equalTo()
//        String email = mFirebaseUser.getEmail();
//        String name = mFirebaseUser.getUid();
//        Toast.makeText(HomeActivity.this, "Email: " + email + " Name; " + name, Toast.LENGTH_SHORT).show();


        btnLogout = findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(v -> {
            FirebaseAuth.getInstance().signOut();
            editor.clear();
            finish();
            startActivity(new Intent(HomeActivity.this, MainActivity.class));
        });
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();
                if (mFirebaseUser == null) {
//                    Toast.makeText(HomeActivity.this,"You are logged in", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(HomeActivity.this, MainActivity.class));
                }
//                else {
//                    Toast.makeText(MainActivity.this,"Please Login", Toast.LENGTH_SHORT).show();
//                }
            }
        };
    }

    public void showData() {
        String nameDB = member.getName();
        String dobDB = member.getDate_of_birth();
        String admitNoDB = member.getAdmit_no();
        String emailDB = member.getEmail();
        String addressDB = member.getAddress();
//        String memberIdDB = member.getMember_id();
        String phoneDB = member.getPhone().toString();
        tvAdmissionText.setText(admitNoDB);
        tvDobText.setText(dobDB);
        tvAddressText.setText(addressDB.length()>0 ? addressDB : "Not Provided");
        tvNameText.setText(nameDB);
        tvEmailText.setText(emailDB);
        tvPhoneText.setText(phoneDB.length()>0 ? "Not Provided" : phoneDB);
//        Toast.makeText(getApplicationContext(), "Default ssssss",Toast.LENGTH_SHORT).show();
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(!(cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected())) {
            Toast.makeText(HomeActivity.this,"No Internet", Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    protected void checkSession() {
        if (mFirebaseAuth.getCurrentUser() == null) {
            finish();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkSession();
        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
    }
}