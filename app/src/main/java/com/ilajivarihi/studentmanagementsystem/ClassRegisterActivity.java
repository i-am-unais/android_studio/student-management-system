package com.ilajivarihi.studentmanagementsystem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ClassRegisterActivity extends AppCompatActivity {

    private SharedPreferences session;
    EditText roomName;
    Button btnAddMember, btnAddSubject, btnSave;
    DatabaseReference dbRoomReff;
    long maxId = 0;
    ClassRoom classRoom;
    private boolean admin;
    private FirebaseUser mFirebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_register);

        session = getSharedPreferences("member", MODE_PRIVATE);
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        dbRoomReff = FirebaseDatabase.getInstance().getReference().child("ClassRoom");
        admin = session.getString("permission", "0").equals("2");
        checkAdminSession();
        dbRoomReff.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    maxId = snapshot.getChildrenCount();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        roomName = (EditText)findViewById(R.id.txtRoomName);
        btnAddMember = findViewById(R.id.btnAddMember);
        btnAddMember.setVisibility(View.GONE);
        btnAddSubject = findViewById(R.id.btnAddSubject);
        btnAddSubject.setVisibility(View.GONE);
        btnSave = findViewById(R.id.btnSave);
        classRoom = new ClassRoom();

        btnSave.setOnClickListener(v -> {
            if (!isNetworkConnected()) {
                Toast.makeText(ClassRegisterActivity.this,"No Internet", Toast.LENGTH_SHORT).show();
                return;
            }

            String name = roomName.getText().toString().trim();
            classRoom.setRoom_name(name);
            classRoom.setRoom_id(maxId + 1);
            if(name.isEmpty()) {
                roomName.setError("Enter Room Name");
                roomName.requestFocus();
            }
            else {
                dbRoomReff.child(String.valueOf(maxId + 1)).setValue(classRoom);
                Toast.makeText(ClassRegisterActivity.this, "Registered Success", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(ClassRegisterActivity.this,MainActivity.class));
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
    protected boolean checkAdminSession() {
        if (mFirebaseUser == null || !admin) {
            FirebaseAuth.getInstance().signOut();
            finish();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            return false;
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkAdminSession();
    }
}