package com.ilajivarihi.studentmanagementsystem;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Map;

public class AttendShowAdapter extends RecyclerView.Adapter<AttendShowAdapter.MyViewHolder> {

    private ArrayList<Member> arrayList;
    private Map<String,Boolean> attendedList;
    private Context context;
//    private int classOpt;

    public AttendShowAdapter(Context ct, ArrayList<Member> arr1, Map<String,Boolean> attList) {
        context = ct;
        arrayList = arr1;
        attendedList = attList;

//        classOpt = option;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.attend_row,parent,false);
        return new AttendShowAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
//        String id = String.valueOf(arrayList.get(position).getClass_id());
        holder.text1.setText(arrayList.get(position).getName());
        String id = arrayList.get(position).getMember_id();
        if (attendedList.get(id)) {
            holder.text2.setText(R.string.present);
            holder.text1.setTextColor(Color.GREEN);
            holder.text2.setTextColor(Color.GREEN);
        } else {
            holder.text2.setText(R.string.absent);
            holder.text1.setTextColor(Color.RED);
            holder.text2.setTextColor(Color.RED);
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    protected static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView text1, text2;
        ConstraintLayout mainLayout;
        ConstraintLayout inneLayout;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            text1 = itemView.findViewById(R.id.name);
            text2 = itemView.findViewById(R.id.tvStatus);
            mainLayout = itemView.findViewById(R.id.mainRow);
            inneLayout = itemView.findViewById(R.id.innerRow);
        }
    }
}

