package com.ilajivarihi.studentmanagementsystem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class StudentActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView;
    private ArrayList<ClassSubject> classSubjects;
    private DatabaseReference dbReff;
    private FirebaseUser firebaseUser;
    private Integer dayOfWeek;
    private SharedPreferences session;
    private SharedPreferences.Editor editor;
    Calendar calendar;
    DateFormat df;
    Intent intent;
    Button logout;
    TextView tvName, tvClass, tvDate;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);

        dbReff = FirebaseDatabase.getInstance().getReference("Subject");
        session = getSharedPreferences("member", MODE_PRIVATE);
        editor = session.edit();
        classSubjects = new ArrayList<>();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        tvName = (TextView)findViewById(R.id.name);
        tvClass = (TextView)findViewById(R.id.tvClass);
        tvDate = (TextView)findViewById(R.id.tvDate);
        logout = findViewById(R.id.logout);

        calendar = Calendar.getInstance();
        dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        df = new SimpleDateFormat("dd-MM-yyyy");
        checkStudentSession();
        getSubs();
    }

    public void getSubs(){
        id = firebaseUser.getUid();
        getMemberDetails();
    }
//    teststudent@gmail.com
    private void getMemberDetails() {
        DatabaseReference dbMember = FirebaseDatabase.getInstance().getReference("Member").child(id);
        dbMember.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String name = snapshot.child("name").getValue(String.class);
                tvName.setText(String.format("%s: %s","Name", name.toUpperCase()));
                tvDate.setText("Date: " + df.format(calendar.getTime()));
                FirebaseDatabase.getInstance().getReference("ClassRoom").child(String.valueOf(snapshot.child("class_room_id").getValue(Long.class)))
                        .addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                tvClass.setText("Class: " + snapshot.child("room_name").getValue(String.class));
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });
                dbReff.orderByChild("class_id").equalTo(snapshot.child("class_room_id").getValue(Long.class)).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        classSubjects.clear();
                        if (snapshot.exists()) {
                            for (DataSnapshot item : snapshot.getChildren()) {
                                ClassSubject sub = item.getValue(ClassSubject.class);
                                if (!sub.getDeleted().equals(1) && !sub.getHidden().equals(1) && sub.getDays().get(dayOfWeek - 1).equals("1")) {
                                    classSubjects.add(sub);
                                }
                            }
                        }
                        if (classSubjects.size() != 0)createView();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        Toast.makeText(getApplicationContext(),"Error Occures! " + error.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getApplicationContext(),"Error Occures! " + error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void createView() {
        MyAdapter myAdapter = new MyAdapter(this, classSubjects, 1);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == logout.getId()) {
            FirebaseAuth.getInstance().signOut();
            intent = new Intent(getApplicationContext(),MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setAction("com.package.ACTION_LOGOUT");
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkStudentSession();
        logout.setOnClickListener(this);
    }

    protected boolean checkStudentSession() {
        if (firebaseUser == null || !session.getString("permission", "0").equals("4")) {
            FirebaseAuth.getInstance().signOut();
            editor.clear();
            finish();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            return false;
        }
        return true;
    }
}