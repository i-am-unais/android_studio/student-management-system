package com.ilajivarihi.studentmanagementsystem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class TutorActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView;
    private ArrayList<ClassSubject> classSubjects;
    private DatabaseReference dbReff;
    private FirebaseUser firebaseUser;
    private Integer dayOfWeek;
    private SharedPreferences session;
    private SharedPreferences.Editor editor;
    Calendar calendar;
    Intent intent;
    Button logout;
    TextView tvName, tvDate;
    DateFormat df;
    String id;
//    ArrayList <String> asdf;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutor);

        dbReff = FirebaseDatabase.getInstance().getReference("Subject");
        session = getSharedPreferences("member", MODE_PRIVATE);
        editor = session.edit();
        classSubjects = new ArrayList<>();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        tvName = (TextView)findViewById(R.id.name);
        tvDate = (TextView)findViewById(R.id.tvDate);
        logout = findViewById(R.id.logout);
        df = new SimpleDateFormat("dd-MM-yyyy");

        calendar = Calendar.getInstance();
        dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        checkTutorSession();
        getSubs();
    }

    public void getSubs(){
        id = firebaseUser.getUid();
        getMemberDetails();
        dbReff.orderByChild("tutor_id").equalTo(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                classSubjects.clear();
                if (snapshot.exists()) {
                    for (DataSnapshot item : snapshot.getChildren()) {
                        ClassSubject sub = item.getValue(ClassSubject.class);
                        if (sub.getDeleted() != 1 && sub.getHidden() !=1 && sub.getDays().get(dayOfWeek - 1).equals("1")) {
                            classSubjects.add(sub);
                        }
                    }
                }
                if (classSubjects.size() != 0)createView();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getApplicationContext(),"Error Occures! " + error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getMemberDetails() {
        DatabaseReference dbMember = FirebaseDatabase.getInstance().getReference("Member").child(id);
        dbMember.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    String name = snapshot.child("name").getValue(String.class);
                    tvName.setText(String.format("%s: %s","Name", name.toUpperCase()));
                    tvDate.setText(String.format("Date: %s", df.format(calendar.getTime())));
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(getApplicationContext(),"Error Occures! " + error.getMessage(),Toast.LENGTH_SHORT).show();
                }
        });
    }

    public void createView() {
        MyAdapter myAdapter = new MyAdapter(this, classSubjects, 0);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == logout.getId()) {
            FirebaseAuth.getInstance().signOut();
            editor.clear();
            finish();
            intent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
//        checkTutorSession();
        logout.setOnClickListener(this);
    }

    protected boolean checkTutorSession() {
        if (firebaseUser == null || !session.getString("permission", "0").equals("3")) {
            FirebaseAuth.getInstance().signOut();
            editor.clear();
            finish();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            return false;
        }
        return true;
    }
}