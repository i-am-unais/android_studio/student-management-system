package com.ilajivarihi.studentmanagementsystem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SAttendActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseUser firebaseUser;
    private SharedPreferences session;
    private SharedPreferences.Editor editor;
    private DatabaseReference dbReff;
    TextView tvName, tvTutor, tvDate;
    Button attendButton;
    private Long subId, classId;
    private DateFormat df;
    private Date dateObj;
    private ClassSubject classSubject;
    private String date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_attend);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        dbReff = FirebaseDatabase.getInstance().getReference();
        session = getSharedPreferences("member", MODE_PRIVATE);
        editor = session.edit();
        df = new SimpleDateFormat("dd-MM-yyyy");
        dateObj = new Date();
        date = df.format(dateObj);

        checkStudentSession();

        attendButton = (Button)findViewById(R.id.attendButton);
        tvName = (TextView)findViewById(R.id.tvName);
        tvTutor = (TextView)findViewById(R.id.tvTutor);
        tvDate = (TextView)findViewById(R.id.tvDate);
        classId = getIntent().getLongExtra("classId", 0);
        subId = getIntent().getLongExtra("subId", 0);

        checkAttend();
    }

    protected void checkAttend() {
        if (subId != 0) {
            dbReff.child("Subject").child(String.valueOf(subId)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()) {
                        classSubject = snapshot.getValue(ClassSubject.class);
                        tvName.setText("Subject: " + classSubject.getSubject_name());
                        tvDate.setText("Date: " + date);

                        if (classSubject.isLock()) {
//                            attendButton.setEnabled(false);
                            attendButton.setText("Closed");
                            attendButton.setClickable(false);
                            attendButton.setBackgroundColor(getColor(R.color.holo_red_light));
                            Toast.makeText(getApplicationContext(),"Attendence Closed", Toast.LENGTH_SHORT).show();
                        } else {
                            if (snapshot.child("attendence").child(date).exists()) {
                                if (snapshot.child("attendence").child(date).child(firebaseUser.getUid()).exists()) {
                                    attendButton.setText("Attended");
                                    attendButton.setBackgroundColor(getColor(R.color.attended));
//                                    attendButton.setEnabled(true);
                                    attendButton.setClickable(false);
                                    Toast.makeText(getApplicationContext(),"Attended", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getApplicationContext(),"Attendence Opened!!!!", Toast.LENGTH_SHORT).show();
                                    attendButton.setText("Attend");
                                    attendButton.setBackgroundColor(getColor(R.color.attend));
//                                    attendButton.setEnabled(true);
                                    attendButton.setClickable(true);
                                }
                            } else {
                                dbReff.child("Subject").child(String.valueOf(subId)).child("lock").setValue(true);
                            }
                        }

                        dbReff.child("Member").child(String.valueOf(classSubject.getTutor_id())).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                tvTutor.setText("Tutor: " + snapshot.child("name").getValue(String.class));
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {
                                Toast.makeText(getApplicationContext(),"Error Occured! "+ error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(getApplicationContext(),"Error Occured! "+ error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getApplicationContext(),"Select subject", Toast.LENGTH_SHORT).show();
        }
    }

    private void attend() {
        dbReff.child("Subject").child(String.valueOf(subId)).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()) {
                            classSubject = snapshot.getValue(ClassSubject.class);
                            if (classSubject.isLock()) {
                                attendButton.setEnabled(false);
                                attendButton.setText("Closed");
                                Toast.makeText(getApplicationContext(),"Attendence Closed", Toast.LENGTH_SHORT).show();
                            } else {
                                dbReff.child("Subject").child(String.valueOf(subId)).child("attendence").child(df.format(dateObj))
                                        .child(firebaseUser.getUid()).setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            attendButton.setText("Attended");
                                            attendButton.setEnabled(true);
                                            attendButton.setClickable(false);
//                                            Toast.makeText(getApplicationContext(),"Attendence updated", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(getApplicationContext(),"Error Occured! "+ task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                });
//                                attendButton.setText("Attended");
//                                attendButton.setEnabled(false);
//                                Toast.makeText(getApplicationContext(),"Attendence updated", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        Toast.makeText(getApplicationContext(),"Error Occured! "+ error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    protected boolean checkStudentSession() {
        if (firebaseUser == null || !session.getString("permission", "0").equals("4")) {
            FirebaseAuth.getInstance().signOut();
            editor.clear();
            finish();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == attendButton.getId()) {
            attend();
            finish();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        checkStudentSession();
        attendButton.setOnClickListener(this);
    }
}