package com.ilajivarihi.studentmanagementsystem;

import androidx.annotation.NonNull;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClassSubject {
    private long subject_id, class_id = 0;
    private String subject_name, startTime, endTime, crdate = currentDate();
    private Integer hidden = 0, deleted = 0;
    private String tutor_id;
    private boolean lock = true;
    private List<String> days = Arrays.asList("0", "0", "0", "0", "0", "0", "0");
//    private List<Attendence> attendenceList;
    // = new HashMap<String, Boolean>();
//    private List<Character>

    public ClassSubject() {
    }

    public long getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(@NonNull long subject_id) {
        this.subject_id = subject_id;
    }

    public Integer getHidden() {
        return hidden;
    }

    public void setHidden(Integer hidden) {
        this.hidden = hidden;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(@NonNull String subject_name) {
        this.subject_name = subject_name;
    }

    public String getTutor_id() {
        return tutor_id;
    }

    public void setTutor_id(@NonNull String tutor_id) {
        this.tutor_id = tutor_id;
    }

//    public String getDays() {
//        return days;
//    }
//
//    public void setDays(@NonNull String days) {
//        this.days = days;
//    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(@NonNull String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(@NonNull String endTime) {
        this.endTime = endTime;
    }

    public long getClass_id() {
        return class_id;
    }

    public void setClass_id(@NonNull long class_id) {
        this.class_id = class_id;
    }

//    public char[] getDayArray() {
//        return this.getDays().toCharArray();
//    }
//
//    public void setDayArray(@NotNull char[] dayChars) {
//        this.setDays(dayChars.toString());
//    }

//    public String getCrdate() {
//        return crdate;
//    }
//
//    public void setCrdate(String crdate) {
//        this.crdate = crdate;
//    }

    protected String currentDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date dateObj = new Date();
        return dateFormat.format(dateObj);
    }


    public List<String> getDays() {
        return days;
    }

    public void setDays(List<String> days) {
        this.days = days;
    }

    public boolean isLock() {
        return lock;
    }

    public void setLock(boolean lock) {
        this.lock = lock;
    }

//    protected static class Attendence {
//        private Map<String, Boolean> attendence;
//        private String date;
//        public Attendence(){
//
//        }
//        public Map<String, Boolean> getAttendence() {
//            return attendence;
//        }
//
//        public void setAttendence(Map<String, Boolean> attendence) {
//            this.attendence = attendence;
//        }
//
//        public String getDate() {
//            return date;
//        }
//
//        public void setDate(String date) {
//            this.date = date;
//        }
//    }
//
//    public List<Attendence> getAttendenceList() {
//        return attendenceList;
//    }
//
//    public void setAttendenceList(List<Attendence> attendenceList) {
//        this.attendenceList = attendenceList;
//    }
}
