package com.ilajivarihi.studentmanagementsystem;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashFunction {
    private BigInteger md5Data = null;
    private MessageDigest messageDigest;
    private static byte[] encriptMD5(String inputBytes) {
        if (!inputBytes.isEmpty()) {
            try {
                MessageDigest messageDigest = MessageDigest.getInstance("MD5");
                messageDigest.update(inputBytes.getBytes());
                return messageDigest.digest();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
        return new byte[0];
    }
    public String getHashString(String text) {
        try {
            md5Data = new BigInteger(1, this.encriptMD5(text));
            text = md5Data.toString(16);
            if (text.length() < 32) {
                text = 0 + text;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return text;
    }

}
