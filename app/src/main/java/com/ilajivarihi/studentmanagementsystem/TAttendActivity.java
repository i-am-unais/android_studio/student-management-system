package com.ilajivarihi.studentmanagementsystem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class TAttendActivity extends AppCompatActivity implements View.OnClickListener {
    private SharedPreferences session;
    private SharedPreferences.Editor editor;
    private DatabaseReference dbReff;
    private ArrayList<Member> students;
    private ClassSubject classSubject;
    private long subId, classId;
    private DateFormat df;
    private Date dateObj;
    private Map<String,Boolean> attendence;
    private FirebaseUser firebaseUser;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private RecyclerView recyclerView;
    Switch aSwitch;
    TextView tvName, tvClass, tvDate;
    ProgressBar progressBar;
    Button button;
    String date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t_attend);

        dbReff = FirebaseDatabase.getInstance().getReference();
        session = getSharedPreferences("member", MODE_PRIVATE);
        editor = session.edit();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        students = new ArrayList<>();
        attendence = new HashMap<String, Boolean>();
        df = new SimpleDateFormat("dd-MM-yyyy");
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        button = (Button)findViewById(R.id.button);
        dateObj = new Date();
        mDateSetListener = (view, year, month, dayOfMonth) -> {
            month = month + 1;
            tvDate.setText(String.format("%d-%d-%d", dayOfMonth, month, year));
        };

        checkTutorSession();
        date = df.format(dateObj);
        aSwitch = (Switch)findViewById(R.id.switchButton);
        tvName = (TextView)findViewById(R.id.tvName);
        tvClass = (TextView)findViewById(R.id.tvClass);
        tvDate = (TextView)findViewById(R.id.tvDate);
        classId = getIntent().getLongExtra("classId", 0);
        subId = getIntent().getLongExtra("subId", 0);
//        getAttendees();

        getSubject();
        getAllStudents();

    }

    private void getAllStudents() {
        students.clear();
//        Toast.makeText(getApplicationContext(),"Start getAllStudents " + classId, Toast.LENGTH_SHORT).show();
        progressBar.setVisibility(View.VISIBLE);
        dbReff.child("Member").orderByChild("class_room_id").equalTo(classId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    for (DataSnapshot item : snapshot.getChildren()) {
                        Member member = item.getValue(Member.class);
                        if (member.getDeleted() == 0 && member.getHidden() == 0 && member.getMember_roll() == 4) {
                            students.add(member);
//                            attendence.put(member.getMember_id(), false);
                        }
                    }
//                    Toast.makeText(getApplicationContext(),"End Loop", Toast.LENGTH_SHORT).show();
                    getAttendees();
                } else {
                    Toast.makeText(getApplicationContext(),"No Students exist in this class room", Toast.LENGTH_SHORT).show();
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getApplicationContext(),"Error Occured! " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getAttendees() {
        attendence.clear();
        String seletedDate = tvDate.getText().toString();
//        Toast.makeText(getApplicationContext(),"date: "+seletedDate,Toast.LENGTH_SHORT).show();
        dbReff.child("Subject").child(String.valueOf(subId)).child("attendence").child(seletedDate)
                .addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()){
                        for (int i=0; i < students.size(); i++) {
                            if (snapshot.child(students.get(i).getMember_id()).exists()) {
                                attendence.put(students.get(i).getMember_id(),true);
                            } else {
                                attendence.put(students.get(i).getMember_id(),false);
                            }
                        }
                        createView();
                    }
                    Toast.makeText(getApplicationContext(),"Class Not Started", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(getApplicationContext(),"Error Occured! " + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
    }

    private void getSubject() {
        tvDate.setText(date);
        dbReff.child("Subject").child(String.valueOf(subId)).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (!snapshot.child("attendence").child(df.format(dateObj)).exists()) {
                    dbReff.child("Subject").child(String.valueOf(subId)).child("lock").setValue(true);
                }
                classSubject = snapshot.getValue(ClassSubject.class);
                tvName.setText("Subject: " + classSubject.getSubject_name());
                dbReff.child("ClassRoom").child(String.valueOf(classId)).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        tvClass.setText("Class: " + snapshot.child("room_name").getValue(String.class));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        Toast.makeText(getApplicationContext(),"Error Occured! " + error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
                if (classSubject.isLock()) {
                    aSwitch.setChecked(false);
                } else {
                    aSwitch.setChecked(true);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getApplicationContext(),"Error Occured! " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void createView() {
        AttendShowAdapter myAdapter = new AttendShowAdapter(this, students, attendence);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == aSwitch.getId()) {
            if (aSwitch.isChecked()) {
//                Toast.makeText(getApplicationContext(),"Checked", Toast.LENGTH_SHORT).show();
                dbReff.child("Subject").child(String.valueOf(subId)).child("lock").setValue(false).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            aSwitch.setChecked(true);
                            dbReff.child("Subject").child(String.valueOf(subId)).child("attendence").child(df.format(dateObj)).child(firebaseUser.getUid())
                                    .addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                                            if (!snapshot.exists()) {
                                                dbReff.child("Subject").child(String.valueOf(subId)).child("attendence").child(df.format(dateObj))
                                                        .child(firebaseUser.getUid()).setValue(true);
//                                                Toast.makeText(getApplicationContext(),"Initialize", Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError error) {
                                            Toast.makeText(getApplicationContext(),"Error Occured! " + error.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                            Toast.makeText(getApplicationContext(),"Attendence Opened", Toast.LENGTH_SHORT).show();
//                            getAttendees();
                        } else {
                            Toast.makeText(getApplicationContext(),"Error Occured! " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } else {
//                Toast.makeText(getApplicationContext(),"Un Checked", Toast.LENGTH_SHORT).show();
                dbReff.child("Subject").child(String.valueOf(subId)).child("lock").setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            aSwitch.setChecked(false);
                            Toast.makeText(getApplicationContext(),"Attendence Closed", Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(getApplicationContext(), "Error Occured! " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
        else if(v.getId() == tvDate.getId()) {
            Calendar calendar = Calendar.getInstance();
            try {
                calendar.setTime(Objects.requireNonNull(df.parse(tvDate.getText().toString())));
            } catch (ParseException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(),"Exception Occured! " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dialog = new DatePickerDialog(TAttendActivity.this,
                    android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                    mDateSetListener,
                    year,month,day
            );
            dialog.getDatePicker().setMaxDate(dateObj.getTime());
            Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        } else if (v.getId() == button.getId()){
            getAllStudents();

            //testtutor@gmail.com
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkTutorSession();
        aSwitch.setOnClickListener(this);
        tvDate.setOnClickListener(this);
        button.setOnClickListener(this);
    }

    protected boolean checkTutorSession() {
        if (firebaseUser == null || !session.getString("permission", "0").equals("3")) {//testtutor@gmail.com
//            getIntent().getBooleanExtra("tutor", false) ||
            FirebaseAuth.getInstance().signOut();
            editor.clear();
            finish();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            return false;
        }
        return true;
    }
}